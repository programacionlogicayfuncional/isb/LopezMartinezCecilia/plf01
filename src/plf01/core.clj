(ns plf01.core)

(defn función-<-1
  [a b] 
  (< a b))

(defn función-<-2
  [a b c ]
  (< a b c ))

(defn función-<-3
  [a b c d]
  (< a b c d))

(función-<-1 (* 10 5) 100)
(función-<-2 100 200.6 300)
(función-<-3 1/8 96 97 98)


(defn función-<=-1
  [a b]
  (<= a b))

(defn función-<=-2
  [a b c]
  (<= a b c))

(defn función-<=-3
  [a b c d e f g h]
  (<= a b c d e f g h))

(función-<=-1 2 3)
(función-<=-2 1/3 1/4 1/8)
(función-<=-3 0.000000001 0.00000001 0.0000001 0.000001 0.00001 0.0001 0.001 0.1)

(defn función-==-1
  [a b]
  (== a b))

(defn función-==-2
  [a b c]
  (== a b c))

(defn función-==-3
  [a b c d]
  (== a b c d))

(función-==-1 3 5)
(función-==-2 1/4 3/4 6/8)
(función-==-3 1 100/100 5000/5000 2/2)


(defn función->-1
  [a b]
  (> a b))

(defn función->-2
  [a b c]
  (> a b c))

(defn función->-3
  [a b c d]
  (> a b c d))

(función->-1 20 10)
(función->-2 1/4 1/8 1/10)
(función->-3 (* 10 2) (* 5 2) (* 2 2) (* 1 1))


(defn función->=-1
  [a b]
  (>= a b))

(defn función->=-2
  [a b c]
  (>= a b c))

(defn función->=-3
  [a b c d ]
  (>= a b c d))

(función->=-1 (count [10 20 30]) (count [1 2]))
(función->=-2 100 50/10 10/10)
(función->=-3 1.0 0.45 0.23 0.45)


(defn función-assoc-1
  [m ll v]
  (assoc m ll v))

(defn función-assoc-2
 [m ll v]
  (assoc m ll v))

(defn función-assoc-3
  [m ll v]
  (assoc m ll v))

(función-assoc-1 nil :key1 4)
(función-assoc-2 {} :nombre "cecy" )
(función-assoc-3 {} :nombre{:nombre "cecy"})

(defn función-assoc-in-1
  [m ll v]
  (assoc-in m ll v))

(defn función-assoc-in-2
  [m ll v]
  (assoc-in m ll v))

(defn función-assoc-in-3
  [m ll v]
  (assoc-in m ll v))

(función-assoc-in-1 [{:name "cecy" :age 26}  {:name "Ema" :age 43}] [1 :age] 44)
(función-assoc-in-2 [{:name "cecy" :age 26}  {:name "Ema" :age 43}] [1 :grade] 10)
(función-assoc-in-3 [{:a 10 :b 26 :c 15}  {:a 10 :b 26 :c 15} {:a 10 :b 26 :c 15}] [2 :a] 1500004)

(defn función-concat-1
  [num1 num2 num3]
  (concat num1 num2 num3))
  
(defn función-concat-2
  [a b c d e f]
   (concat  a b c d e f))

(defn función-concat-3
  [a b ]
  (concat a b))

(función-concat-1 {20 19 2 9} {1 2 5 6}{1 2 3 4})
(función-concat-2 (list 1 2 3 4) [5 6 7 8] {9 10 11 12 13 14} #{15 16 17 18} '(30 40 50) (vector 0 0 0))
(función-concat-3 (hash-set \x \c \b \n) (vector \e \f \g \h))


(defn función-conj-1
  [as b]
  (conj as b))

(defn función-conj-2
  [as b]
  (conj as b))

(defn función-conj-3
  [as b]
  (conj as b))


(función-conj-1 [1 2 3] 4)
(función-conj-2 ["c" "e" "c" "i" "l" "i" ] "a")
(función-conj-3 {1 2 3 4} [5 6])


(defn función-cons-1
  [a b]
  (cons a b))

(defn función-cons-2
  [a b]
  (cons a b))

(defn función-cons-3
  [a b]
  (cons a b))

(función-cons-1 10'(20 30 40 50 60))
(función-cons-2 [1 2] [4 5 6])
(función-cons-3 (list 1 2 3 4) (list 5 6 7 8))

(defn función-contains?-1
  [xs k]
  (contains? xs k))

(defn función-contains?-2
  [xs k]
  (contains? xs k))

(defn función-contains?-3
  [xs k]
  (contains? xs k))

(función-contains?-1 {:a 1} :a)
(función-contains?-2 {:a nil} :a)
(función-contains?-3 {:a 1} :b)

(defn función-count-1
  [xs]
  (count xs ))

(defn función-count-2
  [xs]
  (count xs))

(defn función-count-3
  [xs]
  (count xs))

(función-count-1 [0.0 0.1 0.2 0.3 0.4])
(función-count-2 #{\+ \* \~ \a \x})
(función-count-3 (list 60 70 80))

(defn función-disj-1
  [s]
  (disj s ))

(defn función-disj-2
  [s k]
  (disj s k))

(defn función-disj-3
  [s k ks]
  (disj s k ks))

(función-disj-1 #{10 20 30})
(función-disj-2 #{1 2 3} 2)
(función-disj-3 #{1 2 3} 1 3)


(defn función-dissoc-1
  [m ]
  (dissoc m))

(defn función-dissoc-2
  [m k]
  (dissoc m k))

(defn función-dissoc-3
  [m k ks]
  (dissoc m k ks))

(función-dissoc-1 {:valor1 1 :valor2 2 :valor3 3 :valor4 4})
(función-dissoc-2 {:valor1 1 :valor2 2 :valor3 3 :valor4 4} :valor2)
(función-dissoc-3 {:valor1 1 :valor2 2 :valor3 3 :valor4 4} :valor3 :valor4)


(defn función-distinct-1
  [xs]
  (distinct xs))

(defn función-distinct-2
  [xs]
  (distinct xs))

(defn función-distinct-3
  [xs]
  (distinct xs))

(función-distinct-1 [1 2 1 3 1 4 1 5])
(función-distinct-2 (list 10 10 20 20 30 40 50 60))
(función-distinct-3 (vector \a \a \b \b \c \c \d \d))


(defn función-distinct?-1
  [x]
  (distinct? x))

(defn función-distinct?-2
  [x y]
  (distinct? x y))

(defn función-distinct?-3
  [x y m]
  (distinct? x y m))
 
(función-distinct?-1 1)
(función-distinct?-2 10 20)
(función-distinct?-3 0.1 0.1 0.1)

(defn función-drop-last-1
  [xs]
  (drop-last xs))

(defn función-drop-last-2
  [n xs]
  (drop-last n xs))

(defn función-drop-last-3
  [n xs]
  (drop-last n xs))

(función-drop-last-1 [1 1 1 1 1 1 1 1])
(función-drop-last-2 1 [[1 2] [1 2] [3 4]])
(función-drop-last-3 2 [[1 2] [1 2] [3 4] #{2} #{0}])

(defn función-empty-1
  [xs]
  (empty xs))

(defn función-empty-2
  [xs]
  (empty xs))

(defn función-empty-3
  [xs]
  (empty xs))

(función-empty-1 [])
(función-empty-2 (list 10 20 30 40 50 60))
(función-empty-3 [[10 20] [01 20] [03 04]#{10 20}])

(defn función-empty?-1
  [a]
  (empty? a))

(defn función-empty?-2
  [a ]
  (empty? a ))

(defn función-empty?-3
  [a]
  (empty? a))

(función-empty?-1 [])
(función-empty?-2 {})
(función-empty?-3 [1 2 3 4 4 5])

(defn función-even?-1
  [a ]
  (even? a))

(defn función-even?-2 
  [num]
(even? num))

(defn función-even?-3
  [a ]
  (even? a))

(función-even?-1 3)
(función-even?-2 2)
(función-even?-3 100)


(defn función-false?-1
   [datoBoleano]
   (false? datoBoleano))

(defn función-false?-2
  [datoBoleano]
  (false? datoBoleano))

(defn función-false?-3
  [datoBoleano]
  (false? datoBoleano))

(función-false?-1 true)
(función-false?-2 false)
(función-false?-3 nil)


(defn función-find-1
  [m k]
  (find m k))

(defn función-find-2
  [m k]
  (find m k))

(defn función-find-3
  [m k]
  (find m k))

(función-find-2 {:variable "variable"} :variable)
(función-find-1 {:nombre "cecy" :apllido "López" :edad 22} :edad)
(función-find-3 {:Pais "Mexico" :estado "Oaxaca de Juarez" :region "valles centrales"} :region)


(defn función-first-1
  [xs]
  (first xs))

(defn función-first-2
  [xs]
  (first xs))

(defn función-first-3
  [xs]
  (first xs))

(función-first-1 {\a \b \c \f})
(función-first-2 #{1 2 3 4 5})
(función-first-3 [10 20 30 40 50 60])

(defn función-flatten-1
  [a]
  (flatten a))

(defn función-flatten-2
  [a]
  (flatten a))

(defn función-flatten-3
  [a]
  (flatten  a))

(función-flatten-1 {\a \b \c \f})
(función-flatten-2 [10 20 30 40 50 60 10 [10 20 20]])
(función-flatten-3 (list 10 20 30 {40 50} [20 20]))

(defn función-frequencies-1
  [a]
  (frequencies a))

(defn función-frequencies-2
  [a]
  (frequencies a))

(defn función-frequencies-3
  [a]
  (frequencies  a))

(función-frequencies-1  {\a \b \c \f})
(función-frequencies-2 [10 20 30 40 50 60 10 [10 20 20]])
(función-frequencies-3 (list 10 20 30 {40 50} [20 20]))


(defn función-get-1
  [m k]
  (get m k))

(defn función-get-2
  [m k]
  (get m k))

(defn función-get-3
  [m k]
  (get m k))

(función-get-1 {:valor1 10} :valor1)
(función-get-2 {20 30 40 50 80 100} 20)
(función-get-3 [\a \b \c \d] 2)


(defn función-into-1
  [a]
  (into a))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into a b))

(función-into-1  {1 2 3 4 5 6 7 8 9 10})
(función-into-2 [] {1 2 3 4 5 6 7 8 9 10})
(función-into-3 () [1 10 2 20 3 30 4 40])

(defn función-key-1
  [a]
  (map key a))

(defn función-key-2
  [f as bs]
  (map f as bs))

(defn función-key-3
  [f as bs cs]
  (map f as bs cs))

(función-key-1 {:a 1 :b 2})
(función-key-2 + [1 2 3] [4 5 6])
(función-key-3 - [10 20 30] [1 2 3] [1 2 3])

(defn función-keys-1
  [m]
  (keys m))

(defn función-keys-2
  [m]
  (keys m))

(defn función-keys-3
  [m]
  (keys m))

(función-keys-1 {:a 1 :b 2})
(función-keys-2 {1 10 2 20 3 30 4 40 5 50})
(función-keys-3 (hash-map 10 20 30 40 50 60))

(defn función-max-1
  [a]
  (max a))

(defn función-max-2
  [a b ]
  (max a b))

(defn función-max-3
  [a b c]
  (max a b c))

(función-max-1 100)
(función-max-2 100 20000)
(función-max-3 1 0.00000001 0.001)

(defn función-merge-1
  [a b]
  (merge a b))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b c]
  (merge a b c))

(función-merge-1 {1 2 3 4 5 6} {7 8 9 0})
(función-merge-2 {20 10 30 40} {\a \b \c \d})
(función-merge-3 {20 10 30 40} {\a \b \c \d} {20 10 30 40})

(defn función-min-1
  [a]
  (min a))

(defn función-min-2
  [a b]
  (min a b))

(defn función-min-3
  [a b c]
  (min a b c))

(función-min-1 10)
(función-min-2 100 50)
(función-min-3 1000 0.000001 10)

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a]
  (neg? a ))

(defn función-neg?-3
  [a ]
  (neg? a ))

(función-neg?-1 100)
(función-neg?-2 -0.01)
(función-neg?-3 0)

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [a]
  (nil? a))

(defn función-nil?-3
  [a]
  (nil? a))

(función-nil?-1 nil)
(función-nil?-2 0)
(función-nil?-3 true)

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

(función-not-empty-1 [1 2 3 4])
(función-not-empty-2 {20 30 40 50 60 70})
(función-not-empty-3 '())

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b ]
  (nth a b))

(defn función-nth-3
  [a b]
  (nth a b))

(función-nth-1 ["a" "b" "c" "d"] 0)
(función-nth-2 (vector "a" "b" "c" "d") 3)
(función-nth-3 [(vector 10 20 30) (vector 40 50 60) (vector 70 80 90)]  0)

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

(función-odd?-1 1)
(función-odd?-2 100)
(función-odd?-3 0)

(defn función-partition-1
  [a as]
  (partition a as))

(defn función-partition-2
  [n s xs]
  (partition n s xs))

(defn función-partition-3
  [n s xs]
  (partition n s xs))

(función-partition-1 2 [1 2 3 4 5])
(función-partition-2 3 2 [10 20 30 40 50 60 70 80 90 100])
(función-partition-3 3 2 (list 10 20 30 40 50 60 70 80 90 100))

(defn función-partition-all-1
  [a b]
  (partition-all a b ))

(defn función-partition-all-2
  [a b ]
  (partition-all a b))

(defn función-partition-all-3
  [a b c]
  (partition-all a b c))

(función-partition-all-1 4 4)
(función-partition-all-2 2 [1 2 3 4 5 6 7])
(función-partition-all-3 2 4 [10 20 30 40 50 60 70 80 90 00])

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a ]
  (peek a ))

(función-peek-1 [1 2 3 4 5 6 7 8])
(función-peek-2 (vector 10 20 30 40 50 60 70))
(función-peek-3 (list 10 20 30))

(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [a]
  (pop a))

(defn función-pop-3
  [a]
  (pop a))

(función-pop-1 [1 2 3])
(función-pop-2 '(0.0 0.1 0.2 0.3 0.4 0.5))
(función-pop-3 (vector \a \b \c \d \e \f \g \h))

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a]
  (pos? a))

(defn función-pos?-3
  [a]
  (pos? a))

 (función-pos?-1 100)
(función-pos?-2 -100)
(función-pos?-3 0)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (quot  a b))

(defn función-quot-3
  [a b]
  (quot a b))

(función-quot-1 20 3)
(función-quot-2 100 2)
(función-quot-3 500 10)
 
 (defn función-range-1
   []
   (range ))

(defn función-range-2
  [a b]
  (range  a b))

(defn función-range-3
  [a b c]
  (range a b c))

(función-range-1)
(función-range-2 10 20)
(función-range-3 10 100 2)

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b]
  (rem a b))

(defn función-rem-3
  [a b]
  (rem a b))

(función-rem-1 100 15)
(función-rem-2 10000000000 100)
(función-rem-3 5 5) 

(defn función-repeat-1
  [a]
  (repeat a))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b]
  (repeat  a b))

(función-repeat-1 4)
(función-repeat-2 3 "x")
(función-repeat-3 10 "cecy")

(defn función-replace-1
  [a b ]
  (replace a b ))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace  a b))

(función-replace-1 {1 2 3 45 6 6} [\a \v \b \h])
(función-replace-2 [10 9 8 7 6] [0 2 4])
(función-replace-3 [10 20 30 40 5 060 70] [1 2 3 4])


(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a]
  (rest a))

(defn función-rest-3
  [a]
  (rest  a))

(función-rest-1 {\s \c \f \g})
(función-rest-2 {10 20 30 40 50 60 70 80 90 0})
(función-rest-3 [1 2 3 4])

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (select-keys a b))

(defn función-select-keys-3
  [a b]
  (select-keys  a b))

(función-select-keys-1 {:num1 1 :num2 2} [:num1])
(función-select-keys-2 [1 2 3 4 5 6] [0 0 2 0 0 3])
(función-select-keys-3 {:nombre "cecy" :apllido "López" :edad 22} [:nombre])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a]
  (shuffle  a))

(función-shuffle-1 (list 1 2 3 200 100 399))
(función-shuffle-2 [[10 20 30 40 5 060 70] [1 2 3 4]])
(función-shuffle-3 (vector \a \b \c \d \e \f \g \h))

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a]
  (sort  a))

(defn función-sort-3
  [a]
  (sort   a))

(función-sort-1 [[10 20 30 40 5 060 70] [1 2 3 4]])
(función-sort-2 (vector \k \b \c \d \e \f \g \h))
(función-sort-3 {:nombre "cecy" :apllido "López" :edad 22})

(defn función-split-at-1
  [a b]
  (split-at a b))

(defn función-split-at-2
  [a b]
  (split-at  a b))

(defn función-split-at-3
  [a b]
  (split-at a b))

(función-split-at-1 2 [1 2 3 4 5 10 20 2 3 1])
(función-split-at-2 7 [1 2 10 2 100 3 400 50])
(función-split-at-3  1 {:nombre "cecy" :apllido "López" :edad 22})

(defn función-str-1
  [a ]
  (str a ))

(defn función-str-2
  [a b ]
  (str  a b))

(defn función-str-3
  [a b c]
  (str  a b c))

(función-str-1 [1 2 3 4])
(función-str-2 "Cecilia" "C")
(función-str-3 (vector \k \b \c \d \e \f \g \h) {:nombre "cecy" :apllido "López" :edad 22} "ceci")

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b]
  (subs  a b))

(defn función-subs-3
  [a b c]
  (subs  a b c))

(función-subs-1 "Cecilia Lopez Martinez" 1)
(función-subs-2 "Cecilia Lopez Martinez" 4)
(función-subs-3 "Cecilia Lopez Martinez" 2 5)

(defn función-subvec-1
  [a b]
  (subvec a b))

(defn función-subvec-2
  [a b]
  (subvec  a b))

(defn función-subvec-3
  [a inicio fin]
  (subvec   a inicio fin))

(función-subvec-1 [1 2 3 4 5 6 7] 2)
(función-subvec-2 (vector \k \b \c \d \e \f \g \h) 3)
(función-subvec-3 (vector \k \b \c \d \e \f \g \h) 0 2)

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take   a b))

(defn función-take-3
  [a b ]
  (take a b ))

(función-take-1 3 '(1 2 3 4 5 6 10 20 30 40))
(función-take-2 1 [10 20 30 40 50 60 70 80 90])
(función-take-3 1 [[10 20 30 40 5 060 70] [1 2 3 4]])

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true?  a))

(defn función-true?-3
  [a]
  (true?   a))

(función-true?-1 true)
(función-true?-2 false)
(función-true?-3 0)

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (zero?  a))

(defn función-zero?-3
  [a]
  (zero?   a))

(función-zero?-1 0)
(función-zero?-2 100)
(función-zero?-3 0.001)

(defn función-val-1
  [a]
  (map val a))

(defn función-val-2
  [a]
  (map val  a))

(defn función-val-3
  [a]
  (map val   a))

(función-val-1 {:a 1 :b 2})
(función-val-2 {:nombre "cecy" :apllido "López" :edad 22})
(función-val-2 (hash-map 10 20 30 40 50 60 70 80))

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals  a))

(defn función-vals-3
  [a]
  (vals   a))

(función-vals-1 {:nombre "cecy" :apllido "López" :edad 22})
(función-vals-2 {:a :b :c :g})
(función-vals-3 {10 20 30 40 50 60})

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap    a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

(función-zipmap-1  [:v1 :v2 :v3 :v4 :v5] [1 2 3 4 5])
(función-zipmap-2 [:nombre :apllido  :edad] ["cecilia" "Lopez" 22])
(función-zipmap-3 {1 2 3 4} {1 2 3 4})